import { createRoot } from 'react-dom';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Root from './routes/root';
import Form from './routes/form';

const root = createRoot(document.getElementById('root'));

root.render(
  <Router>
    <Routes>
      <Route path="/" element={<Root />} />
      <Route path="/form" element={<Form />} />
    </Routes>
  </Router>
);