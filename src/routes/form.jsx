import "../form.css"
export default function Form() {
  return (
    <main>
      <form>
        <div className="form">
          <span className="material-symbols-outlined">fingerprint</span>
          <h2>Login</h2>
          <input type="text" className="username" placeholder="UserName" />
          <input type="text" className="password" placeholder="Password" />
          <a className="link1" href="#">
            Forgot password?
          </a>
          <button className="loginbutton">Login</button>
        </div>
      </form>
    </main>
  );
}