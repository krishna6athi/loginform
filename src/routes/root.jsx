import { Link } from "react-router-dom";
import "../index.css"
export default function Root() {
    return (
        <div className='container'>
            <h1>Welcome to our page</h1>
            <h2>Ready to Join?</h2>
            <Link to="/form">
                <button>Login</button>
            </Link>
            <span>Click to join</span>
        </div>
    );
}







